import {
  createStackNavigator,
} from 'react-navigation';
import {
  reduxifyNavigator,
  createReactNavigationReduxMiddleware,
  createNavigationReducer,
} from 'react-navigation-redux-helpers';
import { combineReducers, createStore, applyMiddleware, compose } from 'redux';
import { composeWithDevTools } from 'remote-redux-devtools';
import { Provider, connect } from 'react-redux';
import React from 'react';
import HomeScreen from './screens/Home';
import loginReducer from './redux/reducers/loginReducer';
import * as firebase from 'firebase';

// import store from './config/store';

const AppNavigator = createStackNavigator({
  Home: { screen: HomeScreen },
  Settings: { screen: HomeScreen }
});

const firebaseConfig = {
  apiKey: "AIzaSyD4PQtRheJ2AmD8B5QMzEtq3W4qSoGowoE",
  authDomain: "histagram-b7bd0.firebaseapp.com",
  databaseURL: "https://histagram-b7bd0.firebaseio.com",
  projectId: "histagram-b7bd0",
  storageBucket: "histagram-b7bd0.appspot.com",
  messagingSenderId: "222605497674"
};

export const navReducer = createNavigationReducer(AppNavigator);

const reducers = combineReducers({
  nav: navReducer,
  login: loginReducer
});

export const middleware = createReactNavigationReduxMiddleware(
  "root",
  state => state.nav,
);

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(
  reducers,
  composeEnhancer(
    applyMiddleware(middleware)
  )
);

const App = reduxifyNavigator(AppNavigator, "root");
const mapStateToProps = (state) => ({
  state: state.nav,
});
const AppWithNavigationState = connect(mapStateToProps)(App);

export default class Root extends React.Component {

  componentWillMount() {
    firebase.initializeApp(firebaseConfig);
  }

  render() {
    return (
      <Provider store={store}>
        <AppWithNavigationState />
      </Provider>
    );
  }
}