export const UPDATE_EMAIL = 'update_email';
export const CLEAR_EMAIL = 'clear_email';
export const UPDATE_PASSWORD = 'update_password';
export const CLEAR_PASSWORD = 'clear_password';
