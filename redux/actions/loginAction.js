import _ from 'lodash';
import {
    UPDATE_EMAIL,
    UPDATE_PASSWORD,
    CLEAR_EMAIL,
    CLEAR_PASSWORD
} from './types';

export const updateEmail = (email) => dispatch => {
	dispatch({ type: UPDATE_EMAIL, payload: email });
};

export const updatePassword = (password) => dispatch => {
	dispatch({ type: UPDATE_PASSWORD, payload: password });
};

export const clearEmail = () => dispatch => {
	dispatch({ type: CLEAR_EMAIL });
};

export const clearPassword = () => dispatch => {
	dispatch({ type: CLEAR_PASSWORD });
};