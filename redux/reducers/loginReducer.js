import {
    UPDATE_EMAIL,
    UPDATE_PASSWORD,
    CLEAR_EMAIL,
    CLEAR_PASSWORD
} from '../actions/types';

const INITIAL_STATE = {
    email: '',
    password: ''
};

export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case UPDATE_EMAIL: {
            return { ...state, email: action.payload };
        }
        case UPDATE_PASSWORD: {
            return { ...state, password: action.payload };
        }
        case CLEAR_EMAIL: {
            return { ...state, email: '' };
        }
        case CLEAR_PASSWORD: {
            return { ...state, password: '' };
        }
        default:
            return state;
    }
};
