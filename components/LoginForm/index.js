import React from "react";
import { View, Text, TouchableOpacity, TextInput, KeyboardAvoidingView, StyleSheet } from "react-native";
import { updateEmail, updatePassword, clearEmail, clearPassword } from '../../redux/actions/loginAction';
import { FormLabel, FormInput, FormValidationMessage, Button } from 'react-native-elements'
import { connect } from 'react-redux';

class LoginForm extends React.Component {

    login() {

    }

    render() {
        return (
            <View style={{ flex: 1, borderColor: 'red', borderWidth: 1 }}>
                <Text style={{ fontSize: 28, fontWeight: 'bold', textAlign: 'center' }}>HISTAGRAM</Text>
                <View>
                    <View>
                        <FormLabel labelStyle={styles.label}>Email</FormLabel>
                        <FormInput
                            value={this.props.email}
                            inputStyle={styles.input}
                            onChangeText={email => this.props.updateEmail(email)}
                        />
                    </View>
                    <View>
                        <FormLabel>Password</FormLabel>
                        <FormInput
                            value={this.props.password}
                            inputStyle={styles.input}
                            onChangeText={password => this.props.updateEmail(password)}
                        />
                        <FormValidationMessage>{''}</FormValidationMessage>
                    </View>
                    <View>
                        <Button
                            color={'white'}
                            backgroundColor={'red'}
                            textStyle={{
                                fontWeight: 'bold'
                            }}
                            raised
                            title='Login'
                        />
                        <Button
                            color={'white'}
                            backgroundColor={'blue'}
                            containerViewStyle={{
                                marginTop: 10
                            }}
                            textStyle={{
                                fontWeight: 'bold'
                            }}
                            raised
                            title='Create'
                        />
                    </View>
                </View>
            </View>
        );
    }
}

const mapStateToProps = state => {
    const { email, password } = state.login;
    return { email, password };
};

export default connect(mapStateToProps, { updateEmail, updatePassword })(LoginForm);

const styles = new StyleSheet.create({
    margin15: {
        marginRight: 15,
        marginLeft: 15,
    },
    label: {
        padding: 0,
        marginTop: 0,
        ...this.margin15
    },
    input: {
        paddingLeft: 10, borderColor: '#e4e4e4', borderWidth: 1
    }
})