import React from 'react';
import { View, Text, TouchableOpacity, KeyboardAvoidingView } from 'react-native';
import LoginForm from '../../components/LoginForm';

class HomeScreen extends React.Component {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <LoginForm />
      </View>
    );
  }
}

export default HomeScreen;
